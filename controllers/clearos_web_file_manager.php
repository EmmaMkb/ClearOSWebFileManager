<?php

/**
 * ClearOs Web File Manager controller.
 *
 * @category   Apps
 * @package    ClearOs_Web_File_Manager
 * @subpackage Views
 * @author     Your name <your@e-mail>
 * @copyright  2013 Your name / Company
 * @license    Your license
 */

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * ClearOs Web File Manager controller.
 *
 * @category   Apps
 * @package    ClearOs_Web_File_Manager
 * @subpackage Controllers
 * @author     Your name <your@e-mail>
 * @copyright  2013 Your name / Company
 * @license    Your license
 */

class Clearos_web_file_manager extends ClearOS_Controller
{
    /**
     * ClearOs Web File Manager default controller.
     *
     * @return view
     */

    function index()
    {
        // Load dependencies
        //------------------

        $this->lang->load('clearos_web_file_manager');

        // Load views
        //-----------

        $this->page->view_form('clearos_web_file_manager', NULL, lang('clearos_web_file_manager_app_name'));
    }
}
