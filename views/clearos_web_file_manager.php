<?php

/**
 * ClearOs Web File Manager controller.
 *
 * @category   Apps
 * @package    ClearOs_Web_File_Manager
 * @subpackage Controllers
 * @author     Your name <your@e-mail>
 * @copyright  2013 Your name / Company
 * @license    Your license
 */


$this->lang->load('clearos_web_file_manager'); ?>

<html>
    <head>

    </head>

    <body>
    <div class="btn-toolbar" role="group" aria-label="Basic example">

        <div class="btn-group mr-2" role="group" aria-label="First group">
            <button type="button" class="btn btn-secondary"><i class="fa fa-file-text-o"></i></button>
            <button type="button" class="btn btn-secondary"><i class="fa fa-folder" aria-hidden="true"></i></button>
        </div>

        <div class="btn-group mr-2" role="group" aria-label="First group">
            <!-- download button -->
            <button type="button" class="btn btn-secondary"><i class="fa fa-download"></i></button>

            <!-- upload button -->
            <button type="button" class="btn btn-secondary"><i class="fa fa-upload"></i></button>
        </div>

        <div class="btn-group mr-2" role="group" aria-label="First group">
            <!-- copy button -->
            <button type="button" class="btn btn-secondary"><i class="fa fa-clipboard"></i></button>

            <!-- cut button -->
            <button type="button" class="btn btn-secondary"><i class="fa fa-scissors"></i></button>

            <!-- edit buttn -->
            <button type="button" class="btn btn-secondary"><i class="fa fa-edit"></i></button>
        </div>

        <!-- hidden button -->
        <button type="button" class="btn btn-secondary"><i class="fa fa-eye-slash"></i></button>

        <!-- compresse button -->
        <button type="button" class="btn btn-secondary"><i class="fa fa-file-archive-o" aria-hidden="true"></i></button>

        <div class="btn-group mr-2" role="group" aria-label="First group">
            <!-- trie asc button -->
            <button type="button" class="btn btn-secondary"><i class="fa fa-sort-amount-asc" aria-hidden="true"></i>
            </button>

            <!-- trie des button -->
            <button type="button" class="btn btn-secondary"><i class="fa fa-sort-amount-desc" aria-hidden="true"></i>
            </button>
        </div>

        <!-- delete button -->
        <button type="button" class="btn btn-danger"><i class="fa fa-trash"></i>Supprimer</button>

    </div>

    <p></p>

    <table id="table" class="table table-dark">
        <thead>
        <tr>
            <td>Nom</td>
            <td>Taille</td>
            <td>Permission</td>
            <td>Derniere modification</td>
        </tr>
        </thead>
        <!-- Les lignes  -->
        <tr>
            <td>home</td>
            <td>120Mo</td>
            <td>rw</td>
            <td>12/02/2019</td>
        </tr>

        <tr>
            <td>text.txt</td>
            <td>12ko</td>
            <td>rw</td>
            <td>12/02/2019</td>
        </tr>


        <tr>
            <td>index.php</td>
            <td>12ko</td>
            <td>rw</td>
            <td>12/02/2019</td>
        </tr>
    </table>
    </nav>
    </body>
</html>
